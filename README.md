![app flow](./flow.svg "app flow")

# ./app/.env
```
db_url=docker_database_container_name 
db_port=5432
db_name=workshop
db_username=postgres
db_password=postgres
server_port=9999
```

# application.properties
```
spring.config.import=optional:file:.env[.properties]
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
spring.datasource.url=jdbc:postgresql://${db_url}:${db_port}/${db_name}
server.port=${server_port}
spring.datasource.username=${db_username}
spring.datasource.password=${db_password}
```

# Dockerfile

IntelliJ IDEA: Maven > app > Lifecycle > package

```dockerfile
FROM openjdk:17
WORKDIR /
ADD ./target/app-1.0-SNAPSHOT.jar app-1.0-SNAPSHOT.jar
EXPOSE 9999
CMD java -jar app-1.0-SNAPSHOT.jar
```

# Database

```powershell
docker rm docker_database_container_name -f
docker rmi postgres -f
docker run -it -p 5432:5432 --name docker_database_container_name -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres postgres
docker exec -it docker_database_container_name psql -U postgres
```

```sql
CREATE DATABASE workshop;

\c workshop

CREATE TABLE public.product
(
    id serial NOT NULL,
    name text NOT NULL,
    price integer NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO public.product VALUES (DEFAULT, 'Chleba', 45);

SELECT * FROM public.product;

\q
```

# Network

```powershell
docker network rm docker_network_name
docker network create docker_network_name
docker network connect docker_network_name docker_database_container_name
```

# App

```powershell
docker rm docker_app_container_name -f
docker rmi docker_app_image_name -f
docker build -t docker_app_image_name .
docker run `
-it `
-p 9999:9999 `
--name docker_app_container_name `
--network docker_network_name `
-e db_url=docker_database_container_name `
-e db_port=5432 `
-e db_name=workshop `
-e db_username=postgres `
-e db_password=postgres `
-e server_port=9999 `
docker_app_image_name
```