package org.example.app.controllers;

import com.google.gson.Gson;
import org.example.app.entities.Product;
import org.example.app.entities.ProductsResponse;
import org.example.app.repositories.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class IndexController {

    private ProductRepository productRepository;

    @PostMapping(value = "/create-product", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String createProduct(@RequestParam MultiValueMap<String, String> paramMap) {
        // https://www.baeldung.com/spring-url-encoded-form-data#1-formhttpmessageconverter-basics
        // Parse form data
        String name = paramMap.getFirst("name");
        String priceString = paramMap.getFirst("price");
        if (priceString == null) {
            priceString = "0";
        }
        int price = Integer.parseInt(priceString);
        System.out.print("POST: Name: " + name + ", Price: " + price + "\n");
        // Create and save new product
        Product product = new Product();
        product.setName(name);
        product.setPrice(price);
        System.out.print(product + "\n");
        this.productRepository.saveAndFlush(product);
        return "<p>Product created. See <a href=\"./get-products\">all products</a>.</p>";
    }

    @GetMapping(value = "/get-products", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String getProducts() {
        // https://stackoverflow.com/a/23183963/9318084
        // return this.productRepository.findAll().stream().map(Object::toString).collect(Collectors.joining(", "));
        // https://stackoverflow.com/a/14229020/9318084
        ProductsResponse productsResponse = new ProductsResponse();
        productsResponse.setProducts(this.productRepository.findAll());
        return new Gson().toJson(productsResponse);
    }

    @GetMapping("/")
    public String getIndex() {
        return """
                    <h1>Create Product</h1>
                    <form action="./create-product" method="post">
                      <input type="text" name="name" id="name" placeholder="Name" required>
                      <input type="number" name="price" id="price" placeholder="Price" required>
                      <input type="submit" value="Post">
                    </form>
                """;
    }
}
