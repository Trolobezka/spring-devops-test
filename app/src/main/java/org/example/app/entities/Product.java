package org.example.app.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
// @Table(name="product")
@Getter
@Setter
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Integer price;

    @Override
    public String toString() {
        // https://stackoverflow.com/a/29140403/9318084
        return getClass().getSimpleName() + "[id=" + this.id + ", name=" + this.name + ", price=" + this.price + "]";
    }

}

